import React from 'react';
import Sidebar from './Sidebar';
import Content from './Content';
import Navbar from './Navbar';
import { Navigate, Outlet } from 'react-router-dom';

import {BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import routes from '../../routes/routes';
const MasterLayout = () => {

    const changeStyle1 = () => {
        if (style == "navbar-nav bg-gradient-primary sidebar sidebar-dark accordion")
        {
            setStyle("navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled1");
        }
        else{
            setStyle("navbar-nav bg-gradient-primary sidebar sidebar-dark accordion")
        }
    };
    return (
        <div>
            <div id="wrapper">
                <Sidebar/>
                <div id="content-wrapper" className="d-flex flex-column">

                    <div id="content">
                        <Navbar/>
                        <main>
                            <Routes>
                                {routes.filter(route => route.component)
                                .map(({ path, component: Component }, idx) => (
                                    <Route
                                    key={idx}
                                    path={path}
                                    element={<Component />}
                                    />
                                ))}
                                <Route
                                path="/"
                                element={<Navigate to="/admin/dashboard"/>}
                                />
                            </Routes>
                        </main>

                        
                    </div>
                </div>
            </div>
        </div>
    );
}

export default MasterLayout;
