import React, {useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import { Link, useNavigate } from 'react-router-dom';
import { Dropdown } from 'flowbite-react';
import axios from 'axios';
import Swal from 'sweetalert2';
import logo from '../../images/logo.jpg';
import { NavDropdown } from 'react-bootstrap';

const Navbar = () => {

  const navigate = useNavigate();
  const [propertyList, setPropertyList] = useState([]);
  const [count, setCount] = useState(0);

  let user = localStorage.getItem('auth_name');
  const handleLogout = (e) => {
    e.preventDefault();

    axios.post('api/logout').then(res => {
      if(res.data.status === 200){

        localStorage.removeItem('auth_token');
        localStorage.removeItem('auth_name');
        localStorage.removeItem('wishlist');

        Swal.fire({
          title: "Success",
          text: res.data.message,
          icon: "success"
        });
        navigate("/login");
        window.location.reload();
      }

    });
  }
  useEffect(() =>{
    axios.get('api/front/all-type').then(res=>{
        if(res.data.status === 200){
            setPropertyList(res.data.message);
        }
      });

    const fetchCount = async () => {
      try {
          const response = await axios.get('/api/wishlist/count');
          setCount(response.data.count);
      } catch (error) {
          console.error('Error fetching wishlist count:', error);
      }
    };
    fetchCount();

  },[]);

  var AuthButtons = ' ';
  if(!localStorage.getItem('auth_token')){

    AuthButtons = (
      <>
        <Link to='/login'>
          <button className="me-3 btn-login" type="submit">Login</button>
        </Link>
        <Link to='/register'>
          <button className="btn-signUp" type="submit">Sign up</button>
        </Link>

      </>
                    
    );
    
  }else{
    AuthButtons = (
      <>
      <div class="dropdown">
        <button class="btn dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
        {user}
        </button>
        <ul class="dropdown-menu dropdown-menu-dark">
          <li><a class="dropdown-item active" href="#">Action</a></li>
          <li><a class="dropdown-item" href="#">Another action</a></li>
          <li><a class="dropdown-item" href="#">Something else here</a></li>
          <li><hr class="dropdown-divider"/></li>
          <li><a class="dropdown-item" href="#" onClick={handleLogout}>Logout</a></li>
        </ul>
      </div>
        
        
      </>
    )

  }
  
    return (
      <div className='main-navbar '>

        <nav class="navbar container navbar-expand-lg">
          <div class="container-fluid">
            <a className="navbar-brand "  href="/">
              <img className="h-logo " src={logo} alt="" />
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <i class="fa-solid fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                  <a class="nav-link" aria-current="page" href="/">Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Property
                  </a>
                  <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href={'/all-property'}>All Property</a></li>
                  {
                      propertyList?.map((item,index) => {
                        return (
                          <Link className="dropdown-item" to={`property/${item.id}/${item.name}`}>{item.name}</Link>
                        )
                      })
                    }
                    
                  </ul>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Services</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Blog</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Contact Us</a>
                </li>
                
              </ul>
              <div className="d-flex" role="search">
                
                <a class="nav-link" href="#">Wishlist <i class="fa-solid fa-heart"></i> <span className='count-wish'>{count}</span></a>
                  
                {AuthButtons}
              </div>
            </div>
          </div>
        </nav>

      </div>
    );
}

export default Navbar;
