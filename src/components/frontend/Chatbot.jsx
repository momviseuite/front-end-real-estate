import React, { useState } from 'react';
import axios from 'axios';

const Chatbot = () => {
    const [messages, setMessages] = useState([]);
    const [userMessage, setUserMessage] = useState('');
    const [isChatOpen, setIsChatOpen] = useState(true);

    const handleChatToggle = () => {
        setIsChatOpen(!isChatOpen);
    };

    const sendMessage = async () => {
        if (userMessage.trim() === '') return;

        const newMessages = [...messages, { role: 'user', content: userMessage }];
        setMessages(newMessages);

        try {
            const response = await axios.post('/api/chatbot', { message: userMessage });
            setMessages([...newMessages, { role: 'bot', content: response.data.response }]);
        } catch (error) {
            console.error('Error sending message:', error);
        }

        setUserMessage('');
    };

    const renderMessage = (msg, index) => {
        switch (msg.role) {
          case 'user':
            return (
                <div key={index} class="d-flex flex-row justify-content-end mb-4">
                    <div class="p-2 ms-1" >
                        <p class="small mb-0"><strong>User:</strong> {msg.content}</p>
                    </div>
                    <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava2-bg.webp"
                    alt="avatar 1" width={45} height={45}/>
                </div>
            );
          case 'bot':
            return (
              
                <div key={index} className="bot" class="d-flex flex-row justify-content-start mb-3">
                    <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava1-bg.webp"
                        alt="avatar 1" width={45} height={45} />
                    <div class="p-2 ms-1" >
                        <p class="small mb-0"><strong>ChatBot:</strong> {msg.content}</p>
                    </div>
                </div>
              
            );
          default:
            return (
              <div key={index} className="unknown">
                <strong>Unknown:</strong> {msg.content}
              </div>
            );
        }
    };

    return (
        <div className="row">
            <div id="Smallchat">
            <div className={`Layout ${isChatOpen ? 'Layout-open Layout-expand' : ''} Layout-right`}>
                <div className="Messenger_messenger">
                <div className="Messenger_header">
                    <h4 className="Messenger_prompt">How can we help you?</h4>
                    <span className="chat_close_icon" onClick={handleChatToggle}>
                    <i className="fa fa-window-close" aria-hidden="true"></i>
                    </span>
                </div>
                <div className="Messenger_content">
                    <div className="Messages">
                    <div className="Messages_list">
                        <div id="chatbox">
                            <div id="messages">
                                <div class="card-body">
                                {messages.map((msg, index) => renderMessage(msg, index))}
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div className="Input Input-blank">
                    <textarea className="Input_field" value={userMessage} 
                        onChange={(e) => setUserMessage(e.target.value)} placeholder="Send a message..."></textarea>
                    <button onClick={sendMessage} className="Input_button Input_button-send">
                        <div className="Icon">
                        <i class="fa-regular fa-paper-plane"></i>
                        </div>
                    </button>
                    </div>
                </div>
                </div>
            </div>
            <div className="chat_on" onClick={handleChatToggle}>
                <span className="chat_on_icon">
                <i className="fa fa-comments" aria-hidden="true"></i>
                </span>
            </div>
            </div>
        </div>
    );
};

export default Chatbot;
