import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Swal from 'sweetalert2';

const WishlistButton = ({ property_id, initialInWishlist }) => {
    const [inWishlist, setInWishlist] = useState(initialInWishlist);

    useEffect(() => {
        setInWishlist(initialInWishlist);
    }, [initialInWishlist]);

    const handleWishlistToggle = async () => {
        try {
            const url = inWishlist ? '/api/wishlist/remove' : '/api/wishlist/add';
            await axios.post(url, { property_id }).then(res =>{
                if(res.data.status === 200){
                    Swal.fire({
                        title: "Success",
                        text: res.data.message,
                        icon: "success"
                    });
                    window.location.reload();
                    setInWishlist(!inWishlist);
                    updateLocalStorage(property_id, !inWishlist);

                }else if(res.data.status === 404){
                    const modal = new bootstrap.Modal(document.getElementById('exampleModal'));
                    modal.show();
                }
            })
            
        } catch (error) {
            console.error('Error updating wishlist', error);
        }
    };

    const updateLocalStorage = (id, value) => {
        const wishlist = JSON.parse(localStorage.getItem('wishlist')) || {};
        wishlist[id] = value;
        localStorage.setItem('wishlist', JSON.stringify(wishlist));
    };

    return (
        <div className="wishlist-box">
            <i className={`wishlist-icon ${inWishlist ? 'in-wishlist' : ''}`}
                    onClick={handleWishlistToggle}>
                    {inWishlist ? '♥' : '♡'}
            </i>
        </div>
    );
};

export default WishlistButton;
