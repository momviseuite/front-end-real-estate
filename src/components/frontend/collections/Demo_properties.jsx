import React, { useEffect, useState } from 'react';
import "../../../assets/css/view_detail.css";
import axios from 'axios';
import { Link, useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import Rating from 'react-rating';
import Ratings from './Ratings';
import PriceComponent from './PriceComponent';
import Modal from './Modal';


const DemoProperties = ({userId}) => {

    const navigate = useNavigate();
    const {id} = useParams();   
    const [loading, setLoading] = useState(true); 
    const [ratings, setRatings] = useState([]);
    const [propertyList, setPropertyList] = useState([]);
    const [latestPost, setLatestPost] = useState([]);
    const [location, setLocation] = useState([]);
    const [rating, setRating] = useState(0);
    const [comments, setComments] = useState([]);
    const [replies, setReplies] = useState({});
    const [comment, setComment] = useState('');
    const [reply, setReply] = useState('');

    const [showModal, setShowModal] = useState(false);
    const [modalTitle, setModalTitle] = useState('');
  
    const [rat,setRat] = useState([]);
    


    const handleCommentSubmit = async (e) => {
        e.preventDefault();
        try {
            
            axios.post('/api/comments', {
                user_id: userId,
                property_id: id,
                body: comment
            }).then(res => {
                if (res.data.status === 200) {
                    Swal.fire({
                        title: "Success",
                        text: res.data.message,
                        icon: "success"
                    });
                    window.location.reload();
        
                    setComment('');
                    setComments([...comments, res.data]);
        
                } else if (res.data.status === 404) {
                    const modal = new bootstrap.Modal(document.getElementById('exampleModal'));
                    modal.show();
                }
        
            })
            
        } catch (error) {
            console.error(error);
        }
    };

    const handleReplySubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.post('/api/replies', {
                user_id: userId,
                comment_id: id,
                body: reply
            });
            console.log(response.data);
            setReply('');
            setReplies({...replies, [id]: [...(replies[id] || []), response.data]});
        } catch (error) {
            console.error(error);
        }
    };


    const handleRatingChange = (value) => {
        
        axios.post('/api/ratings', {
            property_id: id,
            rating: value
        }).then(res => {
            if(res.data.status === 200){
                Swal.fire({
                    title: "Success",
                    text: res.data.message,
                    icon: "success"
                });
                setRating(value);
            }else if(res.data.status === 404){
                const modal = new bootstrap.Modal(document.getElementById('exampleModal'));
                modal.show();
            }
        }).catch(error => {
            console.error(error);
        });

    }

    // wishlist
    const [inWishlist, setInWishlist] = useState(false);
    const handleWishlistToggle = async () => {
        try {
            const url = inWishlist ? '/api/wishlist/remove' : '/api/wishlist/add';
            const response = await axios.post(url, { property_id: id,
             });
    
            if (response.data.status === 200) {
                Swal.fire({
                    title: "Success",
                    text: response.data.message,
                    icon: "success"
                });
                window.location.reload();
    
                const wishlist = JSON.parse(localStorage.getItem('wishlist')) || {};
                if (inWishlist) {
                    delete wishlist[id];
                } else {
                    wishlist[id] = true;
                }
                localStorage.setItem('wishlist', JSON.stringify(wishlist));
                setInWishlist(!inWishlist);
    
            } else if (response.data.status === 404) {
                const modal = new bootstrap.Modal(document.getElementById('exampleModal'));
                modal.show();
            }
    
        } catch (error) {
            console.error('Error updating wishlist', error);
        }
    };
    

    useEffect(() =>{     
            const savedWishlist = JSON.parse(localStorage.getItem('wishlist')) || {};
            setInWishlist(savedWishlist[id] || false);

            axios.get(`api/front/view-property/${(id)}`).then(res=>{
                if(res.data.status === 200){
                    setPropertyList(res.data.message);
                    setLocation(res.data.message);
                    setLoading(false)
                }
                else if(res.data.status === 400){
                    Swal.fire({
                        title: "Warning",
                        text: res.data.message,
                        icon: "warning"
                    });
                    navigate("/");
                }
                else if(res.data.status === 404){
                    Swal.fire({
                        title: "Warning",
                        text: res.data.message,
                        icon: "warning"
                    });
                    navigate("/");
                }
            });

            // comments
            axios.get('api/comments').then(res=>{
                if(res.data.status === 200){
                    setComments(res.data.message);
                }
                setLoading(false);
            })
            // ratting
            axios.get('/api/ratings')
            .then(response => {
                setRatings(response.data);
            })
            .catch(error => {
                console.error(error);
            });

            // rat
            axios.get('/api/rat')
            .then(response => {
                setRat(response.data.average_rating);
            })
            .catch(error => {
                console.error(error);
            });

            // latest property
            axios.get('api/front/view-property').then(res=>{
                if(res.data.status === 200){
                    setLatestPost(res.data.message);
                }
                setLoading(false);
            });

    },[id]);

    return (
        <div className=''>
            <div class="blog-single gray-bg">
                <div class="container">
                        
                        {rat}
                        <Ratings value={rat} />
                    <div class="row align-items-start">
                        <div class="col-lg-8 m-15px-tb">
                            {
                                loading ? (
                                    <>
                                        <span class="placeholder col-6"></span>
                                        <span class="placeholder w-75"></span>
                                        <span class="placeholder w-100"></span>
                                        <span class="placeholder w-100"></span>
                                        <span class="placeholder" width={25}></span>
                                    </>
                                ): (
                                    <>
                                    
                                        {
                                            propertyList.map((item) => {
                                                return (
                                                    <article class="article" key={item.id}>
                                                        <div class="article-img">
                                                        <span className='text-type m-2'>{item.type.name}</span>
                                                        <img src={`http://127.0.0.1:8000/${item.image}`} class="w-100" height={350} alt="..." />
                                                        <div className="wishlist">
                                                            <i className={`wishlist-icon ${inWishlist ? 'in-wishlist' : ''}`}
                                                                    onClick={handleWishlistToggle}>
                                                                    {inWishlist ? '♥' : '♡'}
                                                            </i>
                                                        </div>

                                                        </div>
                                                        <div class="article-title">
                                                            <h6><a href="#">Lifestyle</a></h6>
                                                            <h2>{item.name}</h2>
            
                                                            <Rating
                                                                initialRating={rating}
                                                                onClick={handleRatingChange}
                                                                emptySymbol="far fa-star "
                                                                fullSymbol="fas fa-star text-warning"
                                                                rating={rating}
                                                                className=''
                                                                
                                                            />
                                                            <div class="media">
                                                                <div class="avatar">
                                                                    <img src="https://bootdey.com/img/Content/avatar/avatar1.png" title="" alt="" />
                                                                </div>
                                                                <div class="media-body">
                                                                    <label>Rachel Roth</label>
                                                                    <span>26 FEB 2020</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="article-content">
                                                            <p>Aenean eleifend ante maecenas pulvinar montes lorem et pede dis dolor pretium donec dictum. Vici consequat justo enim. Venenatis eget adipiscing luctus lorem. Adipiscing veni amet luctus enim sem libero tellus viverra venenatis aliquam. Commodo natoque quam pulvinar elit.</p>
                                                            <p>Eget aenean tellus venenatis. Donec odio tempus. Felis arcu pretium metus nullam quam aenean sociis quis sem neque vici libero. Venenatis nullam fringilla pretium magnis aliquam nunc vulputate integer augue ultricies cras. Eget viverra feugiat cras ut. Sit natoque montes tempus ligula eget vitae pede rhoncus maecenas consectetuer commodo condimentum aenean.</p>
                                                            <h4>What are my payment options?</h4>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                            <blockquote>
                                                                <p class="blockquote-footer">Someone famous in <cite title="Source Title">Dick Grayson</cite></p>
                                                            </blockquote>
            
                                                            <h3>Our Agency</h3>
            
                                                            <div className="row">
                                                                <div className="col-12">
                                                                    <div id="testimonial-slider" class="owl-carousel">
                                                                        <div class="testimonial">
                                                                            <div class="pic">
                                                                                <img src={`http://127.0.0.1:8000/${item.agent.image}`} height={150} alt="" />
                                                                            </div>
                                                                            <div class="testimonial-content">
                                                                                <h3 class="title">Mr. {item.agent.name}</h3>
                                                                                <p class="description mt-4">
                                                                                    {item.agent.description}
                                                                                </p>
                                                                                <a target="_blank" className='m-3' href={item.url}>Contact Him</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
            
            
                                                        </div>
                                                        
                                                    </article>
                                                    
                                                )
                                            })
                                        }
                                    </>
                                )
                            }
                            
                            <div class="contact-form mt-4 article-comment">
                                <h4>Location</h4>
                                {
                                    loading ? (
                                        <>
                                            <span class="placeholder col-6"></span>
                                            <span class="placeholder w-75"></span>
                                            <span class="placeholder w-100"></span>
                                            <span class="placeholder" width={25}></span>
                                        </>
                                    ): (
                                        <>
                                            {
                                                location.map((item, index) => (
                                                    <div key={index} className="">
                                                        <iframe
                                                            key={index}
                                                            title={`Map-${index}`}
                                                            width="100%"
                                                            height="450"
                                                            loading="lazy"
                                                            allowFullScreen
                                                            src={`https://maps.google.com/maps?q=${item.lat},${item.lng}&hl=es;&output=embed`}
                                                        ></iframe>
                                                    </div>
                                                ))
                                            }
                                        </>
                                    )
                                }
                                

                            </div>

                        </div>

                        <div class="col-lg-4 m-15px-tb blog-aside">
                            <div class="widget widget-latest-post">
                                <div class="widget-title">
                                    <h3>Latest Post</h3>
                                </div>
                                <div class="widget-body">
                                    {
                                        loading ? (
                                            <>
                                                <span class="placeholder col-6"></span>
                                                <span class="placeholder w-75"></span>
                                                <span class="placeholder w-100"></span>
                                                <span class="placeholder" width={25}></span>
                                            </>
                                        ): (
                                            <>
                                                {
                                                    latestPost.map((item, index) => {
                                                        return (
                                                            <div class="latest-post-aside media">
                                                                <div class="lpa-left media-body">
                                                                    <div class="lpa-title">
                                                                        <h5><a href={`/view_property/${item.id}`}>{item.name}</a></h5>
                                                                    </div>
                                                                    <div class="lpa-meta">
                                                                        <span>
                                                                            {item.location}
                                                                        </span>&emsp;
                                                                        <span class="date" href="#">
                                                                            $<PriceComponent price={item.price}/>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="lpa-right">
                                                                    <a href={`/view_property/${item.id}`}>
                                                                        <img src={`http://127.0.0.1:8000/${item.image}`} class="" width={400} height={60} alt="..." />
                                                                        {/* <img src="https://www.bootdey.com/image/400x200/FFB6C1/000000" title="" alt=""/> */}
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        )
                                                    })
                                                }
                                            </>
                                        )
                                    }
                                    
                                </div>
                            </div>

                            <div class="">
                                <div>
                                    <form onSubmit={handleCommentSubmit}>
                                        {/* <input type="text" value={comment} onChange={(e) => setComment(e.target.value)} />
                                        <button type="submit">Add Comment</button> */}

                                        <div class="form-floating">
                                            <textarea type="text" value={comment} onChange={(e) => setComment(e.target.value)} class="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                            <label for="floatingTextarea">Comments</label>
                                        </div>
                                        {/* <button type="submit " >Add Comment</button> */}
                                        <div class="send comment-btn mt-2">
                                            <button type="submit " class="px-btn theme"><span>Submit <i class="fa-solid fa-arrow-right"></i></span> </button>
                                        </div>

                                    </form>
                                    {comments.map((item) => {
                                        return (
                                            <div key={item.id}>
                                                {/* <p>{item.body}</p> */}

                                                <div class="card p-3 mt-2">
                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <div class="user d-flex flex-row align-items-center">
                                                            <img src="https://i.imgur.com/C4egmYM.jpg" width="30" class="user-img rounded-circle mr-2"/>
                                                            <span><small class="font-weight-bold text-primary">olan_sams</small></span>
                                                        </div>
                                                        <small>3 days ago</small>
                                                    </div>

                                                    <div class="action d-flex justify-content-between mt-2 align-items-center">
                                                        <div class="reply px-4">
                                                            <small class="font-weight-bold">{item.body}</small>
                                                        </div>

                                                        
                                                    </div>
                                                </div>

                                            </div>
                                        )
                                    })}
                                    
                                </div>
                                


                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="toast-container position-fixed bottom-0 end-0 p-3">
                <div id="liveToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-header">
                    <img src="..." class="rounded me-2" alt="..."/>
                    <strong class="me-auto">Bootstrap</strong>
                    <small>11 mins ago</small>
                    <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                    </div>
                    <div class="toast-body">
                    Hello, world! This is a toast message.
                    </div>
                </div>
            </div>
            <Modal/>
        </div>
    );
}

export default DemoProperties;
