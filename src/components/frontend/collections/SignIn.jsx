import axios from 'axios';
import React, {useState, useEffect} from 'react';

function SignIn() {

    const [loginUrl, setLoginUrl] = useState(null);

    useEffect(() => {
        fetch('http://127.0.0.1:8000/api/auth/google/url', {
            headers : {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error('Something went wrong!');
            })
            .then((data) => setLoginUrl( data.url ))
            .catch((error) => console.error(error));
    }, []);

    return (
        <span>
            {loginUrl != null && (
                <a href={loginUrl} className='text-white'>Sign In With Google</a>
            )}
        </span>
    );
}

export default SignIn;