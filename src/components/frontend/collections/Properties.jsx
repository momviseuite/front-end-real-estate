import React, { useEffect, useState } from 'react';
import Images from './Images';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import FormSearch from './FormSearch';


const Properties = () => {
    const {id} = useParams(); 
    const [loading, setLoading] = useState(true);
    const [propertyList, setPropertyList] = useState([]);
    const [property, setProperty] = useState([]);
    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    const [types, setTypes] = useState([]);
    const [typeId, setTypeId] = useState('');


    const handleSearch = async () => {
        try {
            const response = await axios.get('/api/front/search', {
                params: { name,price,type_id: typeId},
            });
            setProperty(response.data.message);
            setPropertyList(response.data.message);
        } catch (error) {
            console.error('Error searching for property:', error);
        }
    };
    

    useEffect(() =>{
        axios.get(`api/front/type-property/${id}`).then(res=>{
            if(res.data.status === 200){
                setPropertyList(res.data.message);
            }
            setLoading(false);
        });

        axios.get(`api/front/all-property/${id}`).then(res=>{
            if(res.data.status === 200){
                setProperty(res.data.message);
            }
        });

        // type
        axios.get(`api/front/all-type`).then(res=>{
            if(res.data.status === 200){
                setTypes(res.data.message);
            }
        });

    },[id]);

    if(loading){
        return (
            <div className="text-center">
                <h4>Loading Property...</h4>
            </div>
        )
    }else if(id) {
        return (
            <div className='main-home'>
                <section className='container'>
                    {/* <FormSearch/> */}
                    {/* <div className='row'>
                        <div className="col-2">
                            <div class="d-flex" role="search">
                                <input type="text" value={name}
                                onChange={(e) => setName(e.target.value)} 
                                class="form-control me-2" placeholder="Find Your Property Here" 
                                aria-label="Search"/>

                                

                                <button onClick={handleSearch} class="btn btn-outline-success" type="submit">Search</button>
                            </div>
                        </div>
                        <div className="col-2">
                            <input type="text" value={price}
                                onChange={(e) => setPrice(e.target.value)} 
                                class="form-control me-2" placeholder="Find Your Property Here" 
                                aria-label="Search"/>
                        </div>
                        <div className="col-2">
                            <select value={typeId} onChange={(e) => setTypeId(e.target.value)}>
                                <option value="">Select a Type</option>
                                {types.map((type) => (
                                    <option key={type.id} value={type.id}>
                                        {type.name}
                                    </option>
                                ))}
                            </select>
                        </div>
                        
                    </div> */}
                    <div className='row m-auto '>
                        <div className="col-12">
                            <div class="d-flex" role="search">
                                <input type="text" value={name}
                                onChange={(e) => setName(e.target.value)} class="form-control me-2" placeholder="Find Your Property name" aria-label="Search"/>

                                <input type="text" value={price}
                                onChange={(e) => setPrice(e.target.value)} 
                                class="form-control me-2" placeholder="Find Your Property Price" 
                                aria-label="Search"/>

                                <select className='form-search' value={typeId} onChange={(e) => setTypeId(e.target.value)}>
                                    <option value="">Select a Type</option>
                                    {types.map((type) => (
                                        <option key={type.id} value={type.id}>
                                            {type.name}
                                        </option>
                                    ))}
                                </select>
                                <button onClick={handleSearch} class="btn btn-outline-success btn-search" type="submit">Search</button>
                            </div>
                        </div>
                    </div>
                </section>
                
                <section className='container mt-3'>
                    <Images data={propertyList} />
                </section>   
            </div>
        )
        
    } else {
        return (
            // list all property
            <div className='main-home'>
                <section className='container'>
                    {/* <FormSearch/> */}
                    <div className='row m-auto '>
                        <div className="col-12">
                            <div class="d-flex" role="search">
                                <input type="text" value={name}
                                onChange={(e) => setName(e.target.value)} class="form-control me-2" placeholder="Find Your Property name" aria-label="Search"/>

                                <input type="text" value={price}
                                onChange={(e) => setPrice(e.target.value)} 
                                class="form-control me-2" placeholder="Find Your Property Price" 
                                aria-label="Search"/>

                                <select className='form-search' value={typeId} onChange={(e) => setTypeId(e.target.value)}>
                                    <option value="">Select a Type</option>
                                    {types.map((type) => (
                                        <option key={type.id} value={type.id}>
                                            {type.name}
                                        </option>
                                    ))}
                                </select>
                                <button onClick={handleSearch} class="btn btn-outline-success btn-search" type="submit">Search</button>
                            </div>
                        </div>
                    </div>
                </section>
                <section className='container mt-3'>
                    <Images data={property} />
                </section>   
            </div>
        )
    }

    
}

export default Properties;
