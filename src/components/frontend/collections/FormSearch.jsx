import axios from 'axios';
import React, { useState } from 'react';

const FormSearch = () => {

    const [name, setName] = useState('');

    const handleSearch = async () => {
        try {
            const response = await axios.get('/api/front/property/search', {
                params: { name },
            });
            setProperty(response.data.message);
        } catch (error) {
            console.error('Error searching for property:', error);
        }
    };

    return (
        <div className='row'>
            <div className="col">
                <div class="d-flex" role="search">
                    <input type="text" value={name}
                    onChange={(e) => setName(e.target.value)} class="form-control me-2" placeholder="Search" aria-label="Search"/>
                    <button onClick={handleSearch} class="btn btn-outline-success" type="submit">Search</button>
                </div>
            </div>
            
        </div>
    );
}

export default FormSearch;
